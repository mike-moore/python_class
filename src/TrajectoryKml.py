import math, numpy
from lxml import etree
from pykml.factory import KML_ElementMaker as KML
from pykml.factory import GX_ElementMaker as GX
# Example of a Python module with Google Style docstrings:
# Reference for documentation style guide:
# http://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_google.html


class TrajectoryTrace(object):

    def __init__(self, file_name="trajectory.kml"):
        self.__filename__ = file_name
        self.KmlDoc = KML.kml(KML.Document(KML.name(self.__filename__)))
        return

    def AddLandingSite(self, name="trajectory", alt=0, lat=0, lon=0, color='FF0078F0'):
        coordinate_string_ = str(lon) + ',' + str(lat) + ',' + str(alt)
        place_marker_ = KML.Placemark(
            KML.visibility(1),
            KML.description(''),
            KML.Style(
                KML.IconStyle(
                    KML.Icon(
                        KML.href("LandingIcon.png")
                    ),
                ),
            ),
            KML.Point(
                KML.extrude(True),
                KML.altitudeMode("relativeToGround"),
                KML.coordinates(coordinate_string_)
            ),
        )
        self.KmlDoc.Document.append(place_marker_)
        return

    def AddTrajectoryTrace(self, name="trajectory", alt=[], lat=[], lon=[], color='FF0078F0'):
        coordinate_string_ = ''
        for alt, lat, lon in zip(alt, lat, lon):
            coordinate_string_ += str(lon) + ',' + str(lat) + ',' + str(alt) + '\n'
        trace_ = KML.Polygon(
            KML.extrude(0),
            KML.altitudeMode('absolute'),
            KML.outerBoundaryIs(
                KML.extrude(0),
                KML.LinearRing(
                    KML.extrude(0),
                    KML.tessellate(0),
                    KML.altitudeMode('absolute'),
                    KML.coordinates(coordinate_string_),
                ),
            ),
        )
        place_marker_ = KML.Placemark(
            KML.name(name),
            KML.visibility(1),
            KML.description(''),
            KML.Style(
                KML.LineStyle(
                    KML.color(color),
                    KML.width(2.0)
                ),
                KML.PolyStyle(
                    KML.color('00ffffff')
                ),
            ),
            trace_,
        )
        self.KmlDoc.Document.append(place_marker_)
        return

    def SetView(self, lat=0.0, lon=0.0, heading=0.0, tilt=0.0, view_range=0.0):
        look_at_ = KML.LookAt(
            KML.longitude(lon),
            KML.latitude(lat),
            KML.heading(heading),
            KML.tilt(tilt),
            KML.range(view_range),
            GX.altitudeMode('absolute'),
        )
        self.KmlDoc.Document.append(look_at_)

    def WriteFile(self):
        kml_str_ = etree.tostring(self.KmlDoc, pretty_print=True)
        with open(self.__filename__, 'w+') as f:
            f.write(kml_str_.decode('utf-8'))
        return self.__filename__

class Trajectory(object):

    def __init__(self, **kwargs):
        self.Name = kwargs["name"]
        self.Altitudes = numpy.array(kwargs["alt"])
        self.Latitudes = numpy.array(kwargs["lat"])
        self.Longitudes = numpy.array(kwargs["lon"])
        if "convert_degrees" in kwargs.keys():
            self.ConvertToDegrees()
        elif "convert_radians" in kwargs.keys():
            self.ConvertToRadians()

    @property
    def AverageLatitude(self):
        return numpy.mean(self.Latitudes)

    @property
    def AverageLongitude(self):
        return numpy.mean(self.Longitudes)

    @property
    def PeakAltitude(self):
        return numpy.max(self.Altitudes)

    def ConvertToDegrees(self):
        self.Longitudes = numpy.degrees(self.Longitudes)
        self.Latitudes = numpy.degrees(self.Latitudes)

    def ConvertToRadians(self):
        self.Longitudes = numpy.radians(self.Longitudes)
        self.Latitudes = numpy.radians(self.Latitudes)


def add_trajectory(trajectory_kml_object, name, sim_df, color):
    vehicle_trajectory_ = Trajectory(name=name,
                             convert_degrees = True,
                             alt = sim_df["spacecraft.pfix_lla.ned_state.ellip_coords.altitude {m}"],
                             lat = sim_df["spacecraft.pfix_lla.ned_state.ellip_coords.latitude {r}"],
                             lon = sim_df["spacecraft.pfix_lla.ned_state.ellip_coords.longitude {r}"])
    trajectory_kml_object.AddTrajectoryTrace(name=vehicle_trajectory_.Name, alt=vehicle_trajectory_.Altitudes,
                                       lat=vehicle_trajectory_.Latitudes, lon=vehicle_trajectory_.Longitudes,
                                       color=color)
