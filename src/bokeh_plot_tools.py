from __future__ import division
import math
import pandas as pd
import numpy as np
from bokeh import plotting as bp
from bokeh.models import ColumnDataSource, Label, Legend, DatetimeTickFormatter, UndoTool, BoxZoomTool, Span, HoverTool, PanTool
from bokeh.models.glyphs import Line, Ray
from bokeh.palettes import Category10,Blues,Greens,Oranges,Reds,Purples

## Format plots to output into a notebook:
bp.output_notebook()

def sortMethod(val):
    mode = val[0].split()
    return mode[0]

class bokeh_plot:
    '''
    The bokeh_plot class acts as a wrapper for quick plotting functionality
    using the Bokeh package. This class will plot a single pandas DataFrame.
    The x axis of the plot is the index of the data frame, and each column
    appears as a new line in the plot. 
    '''

    ## Define default values for shared class variables:
    height      = 400       # Height of all plots in pixels
    line_width  = 2         # Line width in pixels
    dynamic     = True      # Enable dynamic plot tools
    hover       = False     # Hovertool is disabled by default
    pan         = False     # Pantool is disabled by default
    max_points  = 100000    # Maximum number of points to show in the plot
    resample_seconds = None # Number of seconds to resample to (if above max points)
    y_axis_scaling = 1.5    # when y axis is centered at zero. Scale upper and lower bounds of axis
    horizontal_color = '#000000' #default horizontal band color is black

    def __init__(self,
                 df,                # Dataframe to plot - index will be used as x values
                 title='',          # Plot title 
                 xlabel='',         # X axis label
                 ylabel='',         # Y axis label
                 xdata='',          # String specifying the column name to use for X axis data
                 legend=True,       # Enable or disable legend 
                                    # (Generally should only disable for Monte Carlo runs)
                 dashed=False,      # Make every other line dashed 
                 dynamic=None,      # Set to True or False to enable or disable interative plot behavior 
                                    # (If left as None, class value is used)
                 hover=None,        # Set to True to enable the hover tool
                 pan=None,          # Set to True or False to enable or disable the pan tool
                 zero_center = False, #make main y-axis center at zero
                 palette=Category10[10] # Default color palette
                 ):     

        self.df = df
        self.title = title
        self.xlabel = xlabel
        self.ylabel = ylabel
        self.xdata = xdata
        self.legend = legend
        self.dashed = dashed
        self.dynamic = dynamic
        self.hover = hover
        self.pan = pan
        self.palette = palette
        self.title_align = 'center'
        self.zero_center = zero_center  # set zero_centered flag for show()
        self.axis_2_top = None #set the value for bottom of the second y axis
        self.axis_2_bot = None #set the value for the top of the second y axis

        ## Read in optional arguments from class members if not specified
        if dynamic is None:
            ## If the argument is not supplied by the user, use the class variable value
            self.dynamic = bokeh_plot.dynamic 

        if hover is None:
            self.hover = bokeh_plot.hover

        if pan is None:
            self.pan = bokeh_plot.pan

        ## Set new dataframe index if specified
        if self.xdata:
            self.df = self.df.set_index(self.xdata)

        ## Create Bokeh figure and define behavior    
        if self.df.index.dtype == 'timedelta64[ns]':
            ## Format x axis nicely for a timedelta index
            self.fig = bp.figure(tools=[],plot_height=bokeh_plot.height, x_axis_type="datetime")
            self.fig.xaxis.formatter=DatetimeTickFormatter(days="Day %e",
                                                months="Day %e",
                                                hours="%k:%M:%S",
                                                hourmin="%k:%M:%S",
                                                minutes="%k:%M:%S",
                                                minsec="%k:%M:%S",
                                                seconds="%k:%M:%S",
                                                milliseconds="%k:%M:%S.%3N") 
            self.fig.xaxis.axis_label = 'Time [h:m:s]'
            self.fig.xaxis[0].ticker.desired_num_ticks = 8
        else:
            ## Keep default x_axis_type
            self.fig = bp.figure(tools=[],plot_height=bokeh_plot.height)
            self.fig.xaxis.axis_label = self.df.index.name
        
        ## Add dynamic tools
        if self.dynamic:
            ## Only Undo tool and Box zoom tool are added by default
            self.fig.add_tools(UndoTool(),BoxZoomTool())
            if self.hover:
                if self.df.index.dtype == 'timedelta64[ns]':
                    tooltips = [
                        ("name","$name"),
                        ("x,y","$data_x{%k:%M:%S.%3N}, $data_y"),
                    ]
                    #tooltips = [('Time', '@Time{%k:%M:%S.%3N}')]
                    formatters = {'$data_x' : 'datetime'}
                else:
                    tooltips = [
                        ("name","$name"),
                        ("x,y","$data_x,$data_y"),
                    ]
                    formatters = {}
                self.fig.add_tools(HoverTool(tooltips=tooltips,formatters=formatters))
            if self.pan:
                self.fig.add_tools(PanTool())
        else:
            ## If dynamic=False, disable all toolbar actions
            self.fig.toolbar_location = None
            self.fig.toolbar.active_inspect = None
            self.fig.toolbar.active_drag = None
            self.fig.toolbar.active_scroll = None
            self.fig.toolbar.active_tap = None

        ## Define level-of-detail behavior for better interactive performance
        self.fig.lod_factor = 1000
        self.fig.lod_threshold = 100
        self.fig.toolbar.autohide = True        # Hide plot toolbar when plot not active
        self.fig.sizing_mode = 'stretch_width'  # Stretch plot horizontally to match window

        self.legend_items = []                  # List of legend items to add to plot
        self.lines = []                         # List of lines
        self.colors_used = []                   # List of colors used in plot

        ## Too many points; get rid of some of them.
        if self.df.size > bokeh_plot.max_points: 
            if self.df.index.dtype == 'timedelta64[ns]' and bokeh_plot.resample_seconds:
                print("Resampling to {} seconds.".format(bokeh_plot.resample_seconds))
                self.df = self.df.resample('{}S'.format(bokeh_plot.resample_seconds)).pad()
            else:
                self.df = self.df.iloc[::int(math.ceil(self.df.size / bokeh_plot.max_points))]
            #warnings.warn("bokeh_plot has been downsampled to {} points.".format(self.df.size), stacklevel=1)
            print("bokeh_plot has been downsampled to {} points.".format(self.df.size))

        if self.title:
            self.fig.title.text = self.title

        if self.xlabel:
            self.fig.xaxis.axis_label = self.xlabel
        
        if self.ylabel:
            self.fig.yaxis.axis_label = self.ylabel

        if self.title_align:
            self.fig.title.align = self.title_align

        self.make_plot()


    def make_plot(self):
        return self.add_lines(self.df, append_to_legend=self.legend)
        
    def add_lines(self, df, x_term='', y_terms=[], append_to_legend=False) :
        # If y_terms is not specified use df columns
        if len(y_terms) == 0 :
            y_terms = df.columns
        # If x_term is not set, use whatever we have for xdata... df.index.name otherwise
        if x_term == '' and self.xdata:
            x_term = self.xdata
        if x_term == '' and self.xdata == '':
            x_term = df.index.name
        index_offset = len(self.lines)
        for index, y_term in enumerate(y_terms):
            ii = index+index_offset
            source = ColumnDataSource(df)
            if self.dashed:
                # Change pattern every other line
                if index%2 is 0:
                    color = self.palette[int(ii/2)%len(self.palette)]
                    style = 'solid'
                else: 
                    color = self.palette[int(ii/2-0.5)%len(self.palette)]
                    style = 'dashed'
            else:
                color = self.palette[ii%len(self.palette)]
                style = 'solid'

            self.colors_used.append(color)

            if self.hover:
                line = self.fig.line(x=x_term,
                        y=y_term,
                        source=source,
                        name=y_term,
                        line_width=bokeh_plot.line_width,
                        line_dash=style,
                        line_color=color,
                        hover_line_color='crimson')
            else:
                line = self.fig.step(x=x_term,
                        y=y_term,
                        source=source,
                        name=y_term,
                        line_width=bokeh_plot.line_width,
                        line_dash=style,
                        mode='after',
                        line_color=color)

            self.lines.append(line)

            if append_to_legend:
                # Split units off of column name
                self.legend_items.append( (y_term.rsplit(' ',1)[0], [line]) )

        return self
    
    def set_legend_labels(self, labels):
        # Overwrites the legend text using a passed in list of strings
        self.legend_items = []
        self.legend = True
        for line_number, legend_label in enumerate(labels):
            self.legend_items.append((legend_label, [self.lines[line_number]]))

    def addPhaseViz(self, 
                    dg,                         # Data frame containing moding information.
                                                # Must contain the following columns:
                                                # 'gnc_mode {--}','gnc_mode_name {--}','gnc_phase {1}','gnc_phase_name {--}'                 
                    print_transitions=False     # Print out mode and phase transitions
                    ):
        # Example usage in jupyter notebook----------------------------------
        # variables1 = ['dyn_roll {radian}','dyn_pitch {radian}','dyn_yaw {radian}']
        # variables2 = ['gnc_mode {--}','gnc_mode_name {--}','gnc_phase {1}','gnc_phase_name {--}']
        # df = analysis_data['stg_run']['log_dyn'][variables1]*180/np.pi
        # dg = analysis_data['stg_run']['log_moding'][variables2]
        # bokeh_plot(df,'Vehicle Orientation',ylabel='Angle [deg]').addPhaseViz(dg).show()
        #--------------------------------------------------------------------

        self.dg = dg
        self.show_phases = True

        # Find extrema value to plot
        max_col = np.max( self.df.max(axis=0) )
        min_col = np.min( self.df.min(axis=0) )
        if max_col >= np.abs(min_col):
            self.extrema = max_col
        else:
            self.extrema = min_col

        # Remove colors that are already in use by plot
        base_colors = Category10[10]
        line_color  = []
        for i in range(len(base_colors)):
            if( not(base_colors[i] in self.colors_used) ):
                line_color.append(base_colors[i])

        line_style  = [ '', [5, 3], [2, 2], [5, 3, 1, 3], [1, 2, 1, 2, 3, 2] ]
        mode_list   = []
        mp_list     = []
        phase_items = []

        if(print_transitions):
            print(dg[['gnc_mode_name {--}', 'gnc_phase_name {--}']])

        for i in range(len(self.dg.index)):
            mode_name = self.dg['gnc_mode_name {--}'][i]
            mode_num  = self.dg['gnc_mode {--}'][i]
            phase_name= self.dg['gnc_phase_name {--}'][i]
            phase_num = self.dg['gnc_phase {1}'][i]
            # handle mode info
            if(not (mode_name in mode_list) ):
                mode_list.append(mode_name)
                mp_list.append([])

            # handle phase info
            mode_index = mode_list.index(mode_name)
            if(not (phase_name in mp_list[mode_index]) ):
                mp_list[mode_index].append(phase_name)
                
                color = line_color[mode_index]
                style = line_style[mp_list[mode_index].index(phase_name)]
                vline = Span(location=self.dg.index[i].total_seconds()*1000, dimension='height', line_color=color, line_width=2, line_dash=style, line_alpha=0.65)
                self.fig.add_layout(vline)
                
                # make a fake line to place in legend
                source = ColumnDataSource(self.df)
                line = self.fig.step(x=0,y=self.extrema,source=source,line_color=color,line_width=2,line_dash=style, line_alpha=0.65)
                phase_items.append( (mode_name + " - " + phase_name[:13], [line]) )
            else:
                color = line_color[mode_index]
                style = line_style[mp_list[mode_index].index(phase_name)]
                vline = Span(location=self.dg.index[i].total_seconds()*1000, dimension='height', line_color=color, line_width=2, line_dash=style, line_alpha=0.65)
                self.fig.add_layout(vline)

        # separates modes, but keeps legend colors in order            
        phase_items.sort(key=sortMethod)
        for i in range(len(phase_items)):
            self.legend_items.append(phase_items[i])

        return self

    def add_horizontal_band(self, band, second_axis = False):
        '''
        input is band, a dictionary with {legend_name(s):constant_value}

        when second_axis is true, it will plot on the secondary_axis
        '''
        base_colors = Category10[10]
        dash = 'dashed'
    
        for i, key in enumerate(band.keys()):
            
            color = bokeh_plot.horizontal_color
            if (second_axis):
                hline = Span(location=band[key],dimension='width',line_color=color, line_width=2, 
                                y_range_name='Axis 2',
                                line_dash=dash)
            else:
                hline = Span(location=band[key],dimension='width',line_color=color, line_width=2, 
                                line_dash=dash)

            self.fig.add_layout(hline)
        
        return self

    def add_second_axis(self,df2, col, axis_label = None, zero_center =False, define_axis = False):
        '''
        add a second axis to the graph that will plot on the right side.

        df2 is the dataframe you wish to plot, 
        '''
        max_v = np.amax(df2[col])
        min_v = np.amin(df2[col])

        if (zero_center):
            if abs(max_v) > abs(min_v):
                high_limit = 1.5*max_v
                low_limit = -1.5*max_v
            else:
                high_limit = 1.5*abs(min_v)
                low_limit = -1.5*abs(min_v)   
        
        else:
            if (define_axis):
                high_limit = self.axis_2_top
                low_limit =  self.axis_2_bot 
            else:
                high_limit = max_v +.5*abs(max_v)
                low_limit = min_v - .5*abs(min_v)
        
        
        self.fig.extra_y_ranges = {'Axis 2': Range1d(start = low_limit, end = high_limit )}
        self.fig.add_layout(LinearAxis(
            y_range_name='Axis 2', axis_label = axis_label), 'right')
        source = ColumnDataSource(df2)
        line = self.fig.step(x=df2.index.name,
                        y=col,
                        source=source,
                        name=col,
                        line_width=bokeh_plot.line_width,
                        color = 'purple',
                        y_range_name='Axis 2',
                        )
        self.lines.append(line)

        if self.legend:
            self.legend_items.append( (col, [line]) )


    def show(self):
        # display legends
        if self.legend:
            legend = Legend(items = self.legend_items, location='center')
            self.fig.add_layout(legend, 'right')
            if(self.zero_center):
                if abs(self.max_col_value) > abs(self.min_col_value):
                    top = self.max_col_value * bokeh_plot.y_axis_scaling
                    bot = -self.max_col_value * bokeh_plot.y_axis_scaling
                else:
                    top = abs(self.min_col_value) * bokeh_plot.y_axis_scaling
                    bot = -abs(self.min_col_value) * bokeh_plot.y_axis_scaling                  
                self.fig.y_range=Range1d(bot, top)
            self.fig.legend.click_policy = 'hide' # Enable "click to hide" behavior
        bp.show(self.fig)



if __name__ == '__main__':
    ## Example usage: 
    df = pd.DataFrame({'x':[1,2,3,4],'y1':[2,4,5,6],'y2':[6,5,4,2]})
    df = df.set_index('x')
    bp.reset_output()
    # Option 1 to plot: 
    #bokeh_plot(df,title='test plot',xlabel='x',ylabel='y',legend=True,dashed=True).show()
    # Option 2 to plot:
    fig = bokeh_plot(df,title='test plot',xlabel='x',ylabel='y',legend=True,dashed=True)
    fig.show()
