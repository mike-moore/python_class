"""
Purpose: This a demonstration module intended to introduce the student to basics
         in test driven development with Python. The unit-tests for this module are 
         found in test/test_WarmUps.py.
Author:   Mike Moore
Date:     16 June 2019
"""


def add_two_numbers(a, b):
    return a+b


def concatenate_two_lists(list_a, list_b):
    return list_a+list_b


def square_postive_numbers(list_numbers):
    for num in list_numbers:
        if num < 0:
            raise ValueError()
    return [num*num for num in list_numbers]


def calculate_average(list_numbers):
    avg = 0
    for num in list_numbers:
        avg += num/len(list_numbers)
    return avg


def remove_duplicates_from_list(list_with_dups):
    # Method 1
    list_no_dups = []
    for item in list_with_dups:
        if item not in list_no_dups:
            list_no_dups.append(item)
    return list_no_dups
    # Method 2
    #return list(dict.fromkeys(list_with_dups))


def find_csv_files_in_dir(path_run_dir):
    import glob, os
    found_files_full_paths = glob.glob(os.path.join(path_run_dir, '*.csv'))
    found_files_dict = {}
    for found_file in found_files_full_paths:
        file_names_no_ext = os.path.basename(found_file).split('.')[0]
        found_files_dict[file_names_no_ext] = found_file 
    return found_files_dict


def create_data_frame_from_log_data(log_file):
    import pandas as pd
    return pd.read_csv(log_file)


def create_dict_of_run_data(run_dir):
    import pandas as pd
    run_data = {}
    log_files_dict = find_csv_files_in_dir(run_dir)
    for log_file in log_files_dict.keys():
        run_data[log_file] = pd.read_csv(log_files_dict[log_file])
    return run_data