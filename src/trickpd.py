"""
Purpose: This module provides helpful functions to parse Trick simulation
         runs into Pandas data frames.
Author:   Mike Moore
Date:     16 June 2019
"""
import os, glob, logging, collections, fnmatch
logger = logging.getLogger(__name__)
import pandas as pd


def build_data_frame_from_log_file(log_file):
    logger.info("Reading data from log file : " + log_file)
    log_file_name, ext = os.path.splitext(os.path.basename(log_file))
    if ext == '.csv':
        return load_csv_file(log_file)
    else:
        raise IOError("Log file " + log_file_name + " is an unsupported file type.")


def load_csv_file(log_file):
    trick_df = pd.read_csv(log_file)
    trick_df.index = trick_df['sys.exec.out.time {s}']
    trick_df.index.name = 'Time'
    trick_df.drop('sys.exec.out.time {s}', axis=1, inplace=True)
    return trick_df


def define_analysis_data(defined_runs=[], data_dir='.'):
    analysis_data = collections.OrderedDict()
    for run_alias, run_dir in defined_runs:
        analysis_data[run_alias] = {}
        full_run_dir = os.path.abspath(os.path.join(data_dir, run_dir))
        log_files = glob.glob(os.path.join(full_run_dir, '*.csv'))
        for log_file in log_files:
            log_file_name_no_ext = os.path.basename(log_file).split('.')[0]
            logger.debug('Found data recording group : ' + log_file_name_no_ext + ' for run : ' + run_alias)
            analysis_data[run_alias][log_file_name_no_ext] = build_data_frame_from_log_file(log_file)
    return analysis_data


def load_monte_carlo(mc_dir):
    monte_data = collections.OrderedDict()
    runs = []
    failed_runs = []
    run_map = {}
    for root, dummy, filenames in os.walk(mc_dir):
        for filename in fnmatch.filter(filenames, '*.csv'):
            runs.append(os.path.join(root, filename))
    for run in runs:
        run_name = os.path.basename(os.path.normpath(os.path.join(run, os.pardir)))
        run_map[run_name] = run
    for run in sorted(run_map):
        log_file_name_no_ext = os.path.basename(run_map[run]).split('.')[0]
        logger.debug('Loading data recording group : ' + log_file_name_no_ext + ' for run : ' + run)
        monte_data[run] = {}
        monte_data[run][log_file_name_no_ext] = build_data_frame_from_log_file(run_map[run])
        for var in monte_data[run][log_file_name_no_ext].keys():
            if monte_data[run][log_file_name_no_ext][var].size == 0:
                monte_data.pop(run, None)
                failed_runs.append(run)
                break
    return monte_data, failed_runs
