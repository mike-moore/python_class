# Making the most of this course
The materials found in this repository can help you become a more proficient Python programmer. The course is designed to be 
done in a self-paced fashion within a classroom environment starting from the 
[starting_point branch](https://gitlab.com/mike-moore/python_class/tree/starting_point) of this repository. 
Please feel free to ask the instructor or other students 
questions as you work through the material. Work hard to complete the excercises on your own before looking at the 
[solution branch](https://gitlab.com/mike-moore/python_class/tree/master). Google and Stack Overflow are 
important companions to all professional Python developers, so use both liberally.

## Learning Goals
* Basic Python programming
* Introduction to test driven development
* How to leverage open-source libraries to avoid re-inventing the wheel
* How to write your own Python modules
* Python best practices
* Introductory data science with Pandas and Jupyter Notebook

## Open-source libraries you will use in this course
Python is a useful programming language for performing general engineering tasks. One of the major reasons for this is the large number
of readily available open-source libraries. The powerful combination of the open-source tools
[virtualenv](https://virtualenv.pypa.io/en/stable/) and [pip](https://pypi.org/project/pip/) make it easy to bring in 
modules found on websites like [GitHub](https://github.com/). This allows the developer to focus on their specific task at hand
by relying on general purpose Python modules for much of the heavy lifting. In short, it allows you to be more productive by making use
of contributions from the open-source community.

| Open-Source Library   | Description                                      |
| --------------------- | ------------------------------------------------ |
| [Nose](http://pythontesting.net/framework/nose/nose-introduction/) | Unit testing library for Python. |
| [Pandas](https://pandas.pydata.org/) | Python data analysis library. |
| [NumPy](http://www.numpy.org/) | NumPy is a fundamental math package for scientific computing with Python. |
| [SciPy](https://www.scipy.org/) | A Python-based ecosystem of open-source software for mathematics, science, and engineering. |
| [Matplotlib](https://matplotlib.org/) | Matplotlib is a Python plotting library which produces publication quality figures in a variety of hardcopy formats and interactive environments across platforms. |
| [Jupyter](https://jupyter.org/) | Jupyter Notebook is an open-source web application that allows you to create and share documents that contain live code, equations, visualizations and narrative text. |
| [Bokeh](https://bokeh.org) | Dynamic plotting and data dashboard library optimized for the web. |

## Environment Setup
Follow the [README.md](README.md) for help in setting up your development environment. Make sure you understand and are comfortable with these
steps. Setting up a Python environment for your work is an important part of doing Python development. Ask questions if you get
stuck on anything. [Report an issue](https://gitlab.com/mike-moore/python_class/issues/new) if you find a problem in the course materials,
particularly if you have trouble setting up this course on your own machine. The course was written to be cross platform.

<center>
<img src="docs/imgs/trajectories.png" alt="Spacecraft Trajectories" style="width: 500px;"/>
</center>

# Problem Statement
Use Python and the open-source data science tools Pandas and Jupyter Notebook to create a set of tools that will 
help convey the information captured in your simulations to your customers. Follow best practices for professional 
software development using Python. This implies that your code should be well structured, commented where appropriate, 
and most importantly, tested. The starting_point branch comes with a set of pre-written unit-tests that will fail until 
you write the code to make them pass. Start with the tests found in [tests/test_WarmUps.py](tests/test_WarmUps.py). 
You should write your code in the functions defined in [src/warmups.py](src/warmups.py). Use VS Code to continually 
test and debug while you develop your solution to each problem.

## Suggested Approach
Start by checking out this [video by Microsoft](https://www.youtube.com/watch?v=_C0vbLV6WdA) about a suggested VS Code/Jupyter Notebook workflow.

Once you have done that, feel free to use VS Code to browse through the basics of Python found in the ipynb files within the 
[docs/LectureNotes folder](docs/LectureNotes).

Here's the suggested material that will help you to work through each assignment in this course. 

### Helpful material for test_WarmUps.py
* Python basics: variables, operators, lists, tuples, strings, dictionaries, loops, conditionals, list comprehensions, and functions.
  * Material can be found in [lesson 1](docs/LectureNotes/01.ipynb), [lesson 3](docs/LectureNotes/03.ipynb), [lesson 4](docs/LectureNotes/04.ipynb), [lesson 5](docs/LectureNotes/05.ipynb), and [lesson 6](docs/LectureNotes/06.ipynb)
* String split function
  * Material is in [lesson 4](docs/LectureNotes/04.ipynb).
* The os module, sys module, and path manipulations
  * Brief intro to all three can be found [here](https://thomas-cokelaer.info/tutorials/python/module_os.html)  
* [Exceptions](https://realpython.com/python-exceptions/)  
* The [glob module](https://pymotw.com/2/glob/)  
  * Search directory for files with a given extension.
* Pandas [read_csv](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.read_csv.html). 
  * Convert a CSV file (an ASCII log file) into a Pandas DataFrame
* Bonus: classes in Python
  * Material is in [lesson 7](docs/LectureNotes/07.ipynb).

### Helpful material for test_TrickPd.py
* The [logging module](https://realpython.com/python-logging/)
* [OrderedDicts](https://www.geeksforgeeks.org/ordereddict-in-python/)
* [Pandas DataFrames](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.html)

Of course, you should read through as many of the lessons in the 
[docs/LectureNotes folder](docs/LectureNotes)
as you can at your own leisure. Not all are needed for this course, but they are all helpful towards becoming 
a more knowledgeable Python programmer. 
Try hard to solve this course's exercises on your own. The only real way to learn programming is by writing 
programs.

# Conclusion
If you can make it through all the excercises in this course, you will be well on your way to being a 
proficient Python programmer. If nothing else, hopefully this course convinced you that Python is a 
valuable tool to have in your arsenal. There's a lot to learn, but there are many, many resources out on the web 
that can help you. Use the reference list below as a jumping off point.

## References
Here's a list of useful resources for more information.

1. [Ten Famous Python Applications](http://www.hartmannsoftware.com/Blog/Articles_from_Software_Fans/Most-Famous-Software-Programs-Written-in-Python)
2. [The Incredible Growth of Python](https://stackoverflow.blog/2017/09/06/incredible-growth-python/)
3. [Why Test Driven Development](https://medium.com/@gondy/the-importance-of-test-driven-development-f80b0d02edd8)
4. [How to do Test Driven Development in Python](https://code.tutsplus.com/tutorials/beginning-test-driven-development-in-python--net-30137)
5. [Python IDE Options](http://www.it4nextgen.com/7-best-ides-for-python-programming-in-2018/)
6. [Virtual Environments](https://virtualenv.pypa.io)
7. [Pip](https://pypi.org/project/pip/)
8. [General Python Tutorial](http://www.scipy-lectures.org/intro/language/python_language.html)
8. [Modules and Code Re-Use](http://www.scipy-lectures.org/intro/language/reusing_code.html)
9. [Object Oriented Programming](http://www.scipy-lectures.org/intro/language/oop.html)
10. [Great Matplotlib Tutorial](https://appdividend.com/2019/01/26/matplotlib-tutorial-with-example-python-jupyter-notebook-course/)
11. [Jupyter Notebook Tutorial](https://www.dataquest.io/blog/jupyter-notebook-tutorial/)

## Special Thanks
The material found in the [docs/LectureNotes](docs/LectureNotes) is not mine. I added the GitHub repository found [here](https://github.com/rajathkmp/Python-Lectures) 
as a subfolder to this project. This was done for student convenience when working within this repository. All credit 
for the material in those folders goes to the author below:
* Rajath Kumar - author of [docs/LectureNotes](docs/LectureNotes)

