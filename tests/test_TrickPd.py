from unittest import TestCase
import os, sys, logging, unittest
# Setup logger
logging.basicConfig(level=logging.WARNING)
logging.addLevelName(logging.WARNING, "\033[93m%s\033[1;0m" % logging.getLevelName(logging.WARNING))
logging.addLevelName(logging.ERROR, "\033[91m%s\033[1;0m" % logging.getLevelName(logging.ERROR))
# Setup paths    
this_files_dir = os.path.abspath(os.path.dirname(__file__))
sample_data_dir = os.path.join(this_files_dir, 'sample_data')
sample_configs_dir = os.path.join(this_files_dir, 'sample_configs')
modules_dir = os.path.abspath(os.path.join(this_files_dir, os.pardir, 'src'))
sys.path.append(modules_dir)


class TestTrickPd(TestCase):

    def test_parse_single_log_file(self):
        from trickpd import build_data_frame_from_log_file
        log_file = os.path.join(sample_data_dir, 'log_sample_log_file.csv')
        # The build_data_frame_from_log_file function should return a Pandas data frame
        # containing the data contained in the log_file provided. Time should be used as the
        # DataFrame index.
        df = build_data_frame_from_log_file(log_file)
        self.assertAlmostEqual(df['spacecraft.vehicle_state.total_angle_of_attack {r}'][0], 0.019712, 4)
        return

    def test_parse_single_ascii_run(self):
        from trickpd import define_analysis_data
        # Define a list of tuples that pair a human readable name with a run directory.
        analysis_runs = [
            ('Pad', os.path.join(sample_data_dir, 'RUN_SampleRun1'))
        ]
        # Given the above list of tuples (only one list item in this test case), return a 
        # a dictionary of dictionaries that hold data frames.
        analysis_data_set = define_analysis_data(analysis_runs)
        # Expected dictionary structure has human readable names provided via the passed in analysis_runs tuple as keys.
        # The values are sub-dictionaries that each contain keys for each data recording group (file within the RUN directory). The value for 
        # each data recording group key is the DataFrame containg the simulation data for that particular data recording group.
        df = analysis_data_set['Pad']['log_sample_dr_group1']
        self.assertAlmostEqual(df['spacecraft.vehicle_state.total_angle_of_attack {r}'][0], 0.019712, 4)
        return

    def test_parse_multiple_runs(self):
        from trickpd import define_analysis_data
        # Define a list of tuples that pair a human readable name with a run directory.
        analysis_runs = [
            ('Pad', os.path.join(sample_data_dir, 'RUN_SampleRun1')),
            ('T+01', os.path.join(sample_data_dir, 'RUN_SampleRun2'))
        ]
        # Similar to the test case above, except now we're passing in two different run directories.
        analysis_data_set = define_analysis_data(analysis_runs)
        df = analysis_data_set['Pad']['log_sample_dr_group2']
        self.assertAlmostEqual(df['spacecraft.vehicle_state.total_angle_of_attack {r}'][0], 0.019712, 4)
        df = analysis_data_set['T+01']['log_sample_dr_group2']
        self.assertAlmostEqual(df['spacecraft.vehicle_state.total_angle_of_attack {r}'][0], 0.019712, 4)
        return

    def test_load_monte_carlo(self):
        from trickpd import load_monte_carlo
        mc_dir = os.path.join(sample_data_dir, 'SampleMonteData_100_Runs')
        failed_runs = []
        # Given a monte carlo directory, the load_monte_carlo function should load
        # all of the data recording groups found in all of the sub RUN_ directories
        # and store them in an ordered dict with a structure similar to the one
        # returned by define_analysis_data.
        monte_data, failed_runs = load_monte_carlo(mc_dir)
        df = monte_data['RUN_00000']['log_vehicle_state']
        self.assertAlmostEqual(df['pos_z {m}'].max(), 1871.5856, 4)
        return


if __name__ == '__main__':
    unittest.main()
