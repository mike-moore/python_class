# Import standard modules
import os, sys, logging, argparse
# Setup logger    
logging.basicConfig(level=logging.WARNING)
logging.addLevelName(logging.WARNING, "\033[93m%s\033[1;0m" % logging.getLevelName(logging.WARNING))
logging.addLevelName(logging.ERROR, "\033[91m%s\033[1;0m" % logging.getLevelName(logging.ERROR))
# Setup paths    
this_files_dir = os.path.abspath(os.path.dirname(__file__))
modules_dir = os.path.abspath(os.path.join(this_files_dir, os.pardir, 'src'))
sys.path.append(modules_dir)

# Define command line arguments
def arg_parse():
    # Messages for --help output 
    desc = "This script is used to demonstrate a sample command line application in Python."
    Ex = "Example: python " + sys.argv[0] + " --arg1 2.12 --arg2 3.45"
    # Add items we expect for input from command line
    arg_parser = argparse.ArgumentParser(description=desc, epilog=Ex)
    arg_parser.add_argument("-a", "--arg1", type=float, default=2.12,
            help="First argument to pass into add_two_numbers(arg1, arg2).")
    arg_parser.add_argument("-b", "--arg2", type=float, default=3.45,
            help="First argument to pass into add_two_numbers(arg1, arg2).")
    args = arg_parser.parse_args()
    return args


if __name__ == '__main__':
    args = arg_parse()
    logging.info("Hello world!!!")
    logging.warn("Sample warning message!")
    logging.error("Sample error message!")
    logging.debug("About to call add_two_numbers function")
    logging.debug("Argument one is : " + str(args.arg1))
    logging.debug("Argument two is : " + str(args.arg2))
    from warmups import add_two_numbers
    the_sum = add_two_numbers(args.arg1, args.arg2)
    print_msg =  "The sum of {0:.3f} and {1:.3f} is {2:.3f}".format(args.arg1, args.arg2, the_sum)
    print(print_msg)

