"""
Purpose:  Create KML file for a given trajectory data file
Author:   Mike Moore
Date:     24 June 2021
"""
########################################################################################
# Python module imports
########################################################################################
from __future__ import print_function
import sys, os, logging, argparse
# Used to enable warning/error messages
logging.basicConfig(level=logging.WARNING)
logging.addLevelName(logging.WARNING, logging.getLevelName(logging.WARNING)) # Add Yellow highlight
logging.addLevelName(logging.ERROR, logging.getLevelName(logging.ERROR))     # Add Red highlight
logging.addLevelName(logging.INFO, logging.getLevelName(logging.INFO))
logger = logging.getLogger(__name__)
# Setup paths    
this_files_dir = os.path.abspath(os.path.dirname(__file__))
modules_dir = os.path.abspath(os.path.join(this_files_dir, os.pardir, 'src'))
sys.path.append(modules_dir)

########################################################################################
# Import our modules
########################################################################################
from TrajectoryKml import TrajectoryTrace, add_trajectory
from trickpd import build_data_frame_from_log_file


def arg_parse():
    ########################################################################################
    # Define command line arguments
    ########################################################################################
    argParser = argparse.ArgumentParser()
    argParser.add_argument("-d",  "--data", default=os.path.abspath(os.path.join('sample_data', 'RUN_SampleRun1')), help="Path to directory containing sim data. Typically the RUN_ dir.")
    args = argParser.parse_args()
    return args


def main():
    ########################################################################################
    # Read command line arguments
    ########################################################################################
    args = arg_parse()    

    ########################################################################################
    # Set relevant data directories based on command line arguments
    ########################################################################################
    trick_sim_data_dir = args.data
    print("##########################################################################")
    print("## Generating KML file for trajectory")
    print("##########################################################################")
    print("Loading sim data from : " + trick_sim_data_dir)

    ########################################################################################
    # Load trajectories from NASA and SimWorks
    ########################################################################################
    # NASA data load
    log_file = os.path.join(args.data, 'log_sample_dr_group1.csv')
    sim_data_df = build_data_frame_from_log_file(log_file)

    ########################################################################################
    # Generate Google Earth KML
    ########################################################################################
    try:
        # Define the colors we want to use
        traj_color = 'ffff0000'
        traj_name = 'MyTrajectory'
        # Add the trajectories
        trajectory_kml_ = TrajectoryTrace(file_name=traj_name+'.kml')
        add_trajectory(trajectory_kml_, traj_name, sim_data_df, traj_color)
        trajectory_kml_.WriteFile()
        print("##########################################################################")
        print("## KML file generated : " + trajectory_kml_.__filename__)
        print("##########################################################################")
    except Exception as err:
        logger.error('Error creating KML file:' + str(err))

if __name__ == "__main__":
    main()
