from unittest import TestCase
import os, sys, logging, unittest
# Setup logger    
logging.basicConfig(level=logging.WARNING)
logging.addLevelName(logging.WARNING, "\033[93m%s\033[1;0m" % logging.getLevelName(logging.WARNING))
logging.addLevelName(logging.ERROR, "\033[91m%s\033[1;0m" % logging.getLevelName(logging.ERROR))
# Setup paths    
this_files_dir = os.path.abspath(os.path.dirname(__file__))
modules_dir = os.path.abspath(os.path.join(this_files_dir, os.pardir, 'src'))
sys.path.append(modules_dir)

class TestWarmUps(TestCase):

    def test_add_two_numbers(self):
        from warmups import add_two_numbers
        self.assertAlmostEqual(6.0, add_two_numbers(2.0, 4.0), 4)
        return

    def test_concatenate_two_lists(self):
        from warmups import concatenate_two_lists
        list_a = ['Red', 'Blue', 'Green']
        list_b = ['Yellow', 'Orange']
        expected_list = ['Red', 'Blue', 'Green', 'Yellow', 'Orange']
        self.assertEqual(expected_list, concatenate_two_lists(list_a, list_b))
        return
    
    def test_square_numbers(self):
        from warmups import square_postive_numbers
        first_list_numbers = [4, 0.25, 24, 35]
        first_expected = [16, 0.0625, 576, 1225]
        second_list_numbers = [102, 33.25, 19, 0.5]
        second_expected = [10404, 1105.5625, 361, 0.25]
        self.assertEqual(first_expected, square_postive_numbers(first_list_numbers))
        self.assertEqual(second_expected, square_postive_numbers(second_list_numbers))
    
    def test_error_handling(self):
        from warmups import square_postive_numbers
        list_negative_numbers = [-2.0, -16.0, -5.0]
        with self.assertRaises(ValueError):
            square_postive_numbers(list_negative_numbers)
    
    def test_calculate_average(self):
        from warmups import calculate_average
        test_number_set1 = [5.0, 10.0, 15.0]
        self.assertAlmostEqual(10.0, calculate_average(test_number_set1), 4)
        test_number_set2 = [10.0, 21.0, 46.0, -18.0, 97.0]
        self.assertAlmostEqual(31.20, calculate_average(test_number_set2), 4)
        test_number_set3 = range(1, 1000)
        self.assertAlmostEqual(500.0, calculate_average(test_number_set3), 4)

    def test_remove_duplicates_from_list(self):
        from warmups import remove_duplicates_from_list
        test_number_list = [5.0, 15.0, 0.5, 5.0, 15.0]
        self.assertEqual([5.0, 15.0, 0.5], remove_duplicates_from_list(test_number_list))
        test_string_list = ['red', 'red', 'green', 'blue', 'green']
        self.assertEqual(['red', 'green', 'blue'], remove_duplicates_from_list(test_string_list))

    def test_find_csv_files_in_dir(self):
        from warmups import find_csv_files_in_dir
        this_files_dir = os.path.abspath(os.path.dirname(__file__))
        sample_data_dir = os.path.join(this_files_dir, 'sample_data', 'RUN_SampleRun1')
        # The find_csv_files_in_dir function should take a directory path as input and return
        # a dictionary of the found csv files in that directory where the keys are the 
        # csv file names without the extension and the values are the full path.
        expected_return_dict = {
            'log_sample_dr_group1' : os.path.join(sample_data_dir, 'log_sample_dr_group1.csv'),
            'log_sample_dr_group2' : os.path.join(sample_data_dir, 'log_sample_dr_group2.csv')
        }
        self.assertEqual(expected_return_dict, find_csv_files_in_dir(sample_data_dir))
    
    def test_create_data_frame_from_log_data(self):
        from warmups import create_data_frame_from_log_data
        this_files_dir = os.path.abspath(os.path.dirname(__file__))
        sample_data_dir = os.path.join(this_files_dir, 'sample_data')
        log_data = os.path.join(sample_data_dir, 'log_sample_log_file.csv')
        # Given a path to a log csv file, return a Pandas Data frame
        # containing the data found in the csv file.
        df = create_data_frame_from_log_data(log_data)
        self.assertAlmostEqual(df['spacecraft.vehicle_state.total_angle_of_attack {r}'][0], 0.019712, 4)
    
    def test_read_run_data(self):
        from warmups import create_dict_of_run_data
        this_files_dir = os.path.abspath(os.path.dirname(__file__))
        sample_data_dir = os.path.join(this_files_dir, 'sample_data')
        sample_run_dir = os.path.join(sample_data_dir, 'RUN_SampleRun1')
        # Given a run directory, return a dictionary of Pandas data frames
        # representing all of the log data found in all of the csv files
        # within the given directory. The keys for the returned dictionary
        # should be the log file name with no extension, the values should
        # be the data frame representation of the data in that log file.
        sim_data = create_dict_of_run_data(sample_run_dir)
        dr_group1_df = sim_data['log_sample_dr_group1']
        self.assertAlmostEqual(dr_group1_df['spacecraft.vehicle_state.total_angle_of_attack {r}'][0], 0.019712, 4)
        dr_group2_df = sim_data['log_sample_dr_group2']
        self.assertAlmostEqual(dr_group2_df['spacecraft.vehicle_state.total_angle_of_attack {r}'][0], 0.019712, 4)


if __name__ == '__main__':
    unittest.main()
