# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'
# %%
# ms-python.python added
import os
try:
	os.chdir(os.path.join(os.getcwd(), 'tests/sample_notebook'))
	print(os.getcwd())
except:
	pass

# %% [markdown]
#  <center><img src="https://media3.s-nbcnews.com/j/newscms/2018_03/2294271/180116-deep-space-gateway-boeing_7d3c888417301a38fa3db34b595e0cfc.fit-2000w.jpg" alt="DSG Sim" height="340" width="480"></center><br>
#  <center><font size="6" face="Roboto"><b>Simulation Analysis Report</b></font></center>
#  <center><font size="4" face="Roboto">Demonstration Notebook</font></center><br>
#  <center><i><font size="3" face="Roboto">Mike Moore</font></i></center>
# %% [markdown]
#  # Overview
# 
#  This report serves as a sample Jupyter Notebook to demonstrate a typical simulation analysis workflow.
# 
#  More information can be found at the [Jupyter Notebook](https://jupyter.org/) home page.
# %% [markdown]
#  # Analysis Setup and Configuration
#  Our first step is to setup our analysis environment. We do this by importing the Python modules that we will make use of,
#  and loading the data set we will work with. The cell below imports the modules we will need to carry out this analysis.
#  We add the path to our own Python modules by appending to sys.path.

# %%
# Imports all needed Python modules for this notebook.
import sys, os, logging
import seaborn as sns
from os.path import expanduser
from IPython.display import Image
modules_dir = os.path.abspath(os.path.join(os.getcwd(), os.pardir, os.pardir, 'src'))
sys.path.append(modules_dir)
from trickpd import *
from bokeh_plot_tools import bokeh_plot

# %% [markdown]
#  ## Load Simulation Data
#  Once we have imported our needed modules, the next step is to load our simulation's data. We do this by defining
#  the simulation log file that we will use. The code below shows an example of loading a single log file by defining
#  the path to the file relative to this notebook.

# %%
data_dir = os.path.abspath(os.path.join(os.getcwd(), 'nb_data'))
log_file = os.path.join(data_dir, 'RUN_Demo1', 'log_vehicle_state.csv')
df = build_data_frame_from_log_file(log_file)
monte_data_dir = os.path.abspath(os.path.join(os.getcwd(), os.pardir, 'sample_data'))
mc_dir = os.path.abspath(os.path.join(monte_data_dir, 'SampleMonteData_100_Runs'))
monte_data, failed_runs = load_monte_carlo(mc_dir)

# %% [markdown]
#  ## Examine Data
#  At this stage, our variable 'df' contains a Pandas [DataFrame](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.html)
#  object containing all of our logged simulation data. We can used Pandas to examine some of the data that was loaded in to this Jupyter Notebook.

# %%
n_rows = 10
df.head(n_rows)

# %% [markdown]
#  ## Post-Processing
#  Now that we have our data in a convenient structure, we can leverage the full power of Pandas, NumPy, and Matplotlib to process and
#  analyze the data with ease. Of course, we can create functions, classes, and modules defined in separate Python files if we need to do
#  some complex post-processing. This would allow us to share those modules with other team members that may want to re-use and extend them
#  for a similar analysis. Be sure to test drive these modules as you develop them. As we have seen, the interactive nature of Python makes
#  that easy.
# 
#  As a trivial example, here's how to get the maximum altitude the vehicle achieves. First, we search the data set for altitude related
#  variables and examine those columns.

# %%
altitude_df = df.filter(regex='pos_z {m}')
altitude_df.head(n_rows)

# %% [markdown]
#  Let's get the maximum altitude above Earth (WGS84 co-ordinates) and convert it to feet. We can use Python's
#  [format function](https://pyformat.info/) to print the rounded value out neatly.

# %%
ft_per_meter = 3.28084
altitude_ft = pd.DataFrame({'pos_z {ft}': altitude_df['pos_z {m}'] * ft_per_meter})
max_alt_ft = altitude_ft['pos_z {ft}'].max()
print("The maximum altitude reached is : {:.2f} ft.".format(max_alt_ft))

# %% [markdown]
#  ## Make Some Simple Plots
#  In addition to using Pandas in combination with NumPy for post-processing, we can also use Pandas in
#  combination with Bokeh for plotting. Here's a simple example of plotting a single term.

# %%
plot_obj = bokeh_plot(altitude_ft, 
                    title='Altitude',
                    xlabel='Time (s)',  
                    ylabel='Altitude {ft}',
                    dynamic=True,
                    hover=True,
                    legend=False).show()

# %% [markdown]
# Next we'll co-plot a single variable from two different runs.

# %%
# Load in all data from log_vehicle_state.csv for each RUN
run_demo1_df = build_data_frame_from_log_file(os.path.join(data_dir, 'RUN_Demo1', 'log_vehicle_state.csv'))
run_demo2_df = build_data_frame_from_log_file(os.path.join(data_dir, 'RUN_Demo2', 'log_vehicle_state.csv'))
# Extract only the altitude and convert to ft
run_demo1_altitude_ft_df = pd.DataFrame({'pos_z {ft}': run_demo1_df['pos_z {m}'] * ft_per_meter})
run_demo2_altitude_ft_df = pd.DataFrame({'pos_z {ft}': run_demo2_df['pos_z {m}'] * ft_per_meter})
# Create the plot object.
plot_obj = bokeh_plot(run_demo1_altitude_ft_df, 
                    title='Altitude',
                    xlabel='Time (s)',  
                    ylabel='Altitude {ft}',
                    dynamic=True,
                    hover=True,
                    legend=False)
plot_obj = plot_obj.add_lines(run_demo2_altitude_ft_df)
plot_obj.set_legend_labels(['RUN_demo1', 'RUN_demo2'])
# Render the plot
plot_obj.show()


# %%
# Setup
from bokeh.palettes import viridis
log_file_name = 'log_vehicle_state'
runs = list(monte_data.keys())
var_name_x = 'mach {1}'
var_name_y = 'pos_z {m}'
df = pd.DataFrame()
# Define the plot to make
df = monte_data[runs[-1]][log_file_name][[var_name_x, var_name_y]]
plot_obj = bokeh_plot(df, 
                   title='Mach vs Altitude',
                   xdata=var_name_x,
                   xlabel='Mach (--)',  
                   ylabel='Altitude {m}',
                   palette=viridis(12),
                   dynamic=True,
                   hover=True,
                   legend=False)
# Already plotted the last run in the MC analysis set
runs.pop()
for run in runs:
    df = pd.DataFrame()
    df = monte_data[run][log_file_name][[var_name_x, var_name_y]]
    plot_obj = plot_obj.add_lines(df,
                  x_term=var_name_x,
                  y_terms=[var_name_y])
# Render the plot
plot_obj.show()

# %% [markdown]
# ## Learn More
# The above few plots are just meant to give you just a sense for what is possible with Python, Pandas, and Bokeh. It's really just scratching the surface. There are many references available online. Google is your friend. Be sure to also checkout [bokeh_plot_example.ipynb](bokeh_plot_example.ipynb) for some more simple examples contained within this repository.


# %%


# %%
